import 'package:flutter/material.dart';
import 'package:poc_client/screens/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Poc ao3 components',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Home(),
    );
  }
}
