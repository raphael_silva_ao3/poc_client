import 'package:flutter/material.dart';
import 'package:poc_client/screens/screen_01.dart';
import 'package:poc_flow_sso/poc_flow_sso_home.dart';

class Home extends StatelessWidget {
  const Home();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.blue, title: Text("POC Componentes Flutter")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Screen01()),
              ),
              child: Text("Screen 01"),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => PocFlowSSOHome()),
              ),
              child: Text("FLOW-SSO"),
            ),
          ],
        ),
      ),
    );
  }
}
