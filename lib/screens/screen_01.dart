import 'package:flutter/material.dart';
import 'package:poc_components/widgets/botao_personalizado.dart';
import 'package:poc_components/widgets/container_personalizado.dart';
import 'package:poc_components/widgets/imagem_pesada.dart';

class Screen01 extends StatelessWidget {
  const Screen01();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.blue, title: Text("Screen 01")),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Elementos do package de componentes", style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(height: 10),
                    BotaoPersonalizado(
                      onPressed: () => ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text("BOTÃO CLICADO", textAlign: TextAlign.center),
                          backgroundColor: Colors.purple,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    ContainerPersonalizado(),
                    ImagemPesada(),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 80),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Elementos deste app client", style: TextStyle(fontWeight: FontWeight.bold)),
                    Container(margin: EdgeInsets.only(top: 10), width: 100, height: 100, color: Colors.green),
                    //Container(child: Image.asset("lib/assets/large_image.jpg", package: "poc_components")), ISTO FUNCIONA!!
                    Image.asset("lib/assets/ao3_logo.png"),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
